---
number: 4
title: David Knight interviews Dr. Sherri Tenpenny
cover: tenpenny.jpg
category: "TheRealNews"
author: Josh Cox
date: 2020-04-09
slug: Common Sense
categories:
  - David Knight
  - Dr. Sherri Tenpenny
url: https://2020cdn.enterview.rocks/20200409_Thu_Knight.mp3
---

![](./tenpenny.jpg)

[David Knight](https://www.infowars.com/david-knight-show/) interviews Dr. Sherri Tenpenny

Her web page is [Vaxxter.com](https://vaxxter.com/).

<p><audio controls ><source src='https://2020cdn.enterview.rocks/20200409_Thu_Knight.ogg' type="audio/ogg" /><source src='https://2020cdn.enterview.rocks/20200409_Thu_Knight.ogg' type="audio/mp3" /></audio></p>

Download the [original show](http://rss.infowars.com/20200409_Thu_Knight.mp3).

Banned.video has a channel for David Knight's show [The Real News](https://banned.video/channel/5b92d71e03deea35a4c6cdef).
