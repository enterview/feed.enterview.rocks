---
number: 3
title: David Knight interviews Catherine Austin Fitts
category: "TheRealNews"
cover: catherine_austin_fitts_700_pixels.png
author: Josh Cox
date: 2020-04-03
slug: Common Sense
categories:
  - David Knight
  - Catherine Austin Fitts
url: https://2020cdn.enterview.rocks/20200403_Fri_Knight.mp3
---
David Knight interviews Catherine Austin Fitts
