---
number: 6
title: Judy Mikovits interviewed by Mikki Willis
cover: judymikovits.jpg
category: "Plandemic"
author: Josh Cox
date: 2020-05-04
slug: Plandemic
categories:
  - Judy Mikovits
  - Mikki Willis
  - plandemic
url: https://2020cdn.enterview.rocks/PLANDEMICPart1DrJudyMikovits-fsi9csLNb-Y.mp3
---

![](./judymikovits.jpg)

This is a excerpted interview from the [Plandemic Movie](https://plandemicmovie.com/) which is coming out this summer (2020).

[D.tube link](https://d.tube/v/veritas11/QmYG5y53VFjjHP2t5ZqkH8xoYSMyHwbA1pSHyX3fFt6gw7)
