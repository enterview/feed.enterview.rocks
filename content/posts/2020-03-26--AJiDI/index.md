---
number: 1
title: Alex Jones interviews David Icke
category: "infowars"
slug: There's a war on for your mind
categories:
  - Infowars
  - David Icke
  - Alex Jones
cover: davidicke.jpg
author: Josh Cox
date: 2020-03-26
url: https://20200326.enterview.rocks/20200326_Alex_Jones_interviews_David_Icke.mp3
---
David Icke interviewed by Alex Jones.
