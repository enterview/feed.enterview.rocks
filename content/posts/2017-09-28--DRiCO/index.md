---
number: 7
title: Candace Owens interviewed by David Rubin
cover: rubinreportcandaceowens.jpg
category: "RedPillBlack"
author: Josh Cox
date: 2017-09-28
slug: RubinReport
categories:
  - Candace Owens
  - RedPillBlack
  - David Rubin
url: https://2020cdn.enterview.rocks/OnHerJourneyFromLefttoRight_CandaceOwens__RubinReport-BSAoitd1BTQ.mp3
---

![](./rubinreportcandaceowens.jpg)

This is a flashback video to get a historical perspective of how Candace Owens made her transition from a liberal democrat journalist to a moderate conservative.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BSAoitd1BTQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
